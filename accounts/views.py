from django.shortcuts import redirect, render
from .forms import LoginForm, SignupForm
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib import messages


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect(reverse("list_projects"))
            else:
                form = LoginForm()
                context = {"form": form}
                html_template = loader.get_template("accounts/login.html")
                return HttpResponse(html_template.render(context, request))
        else:
            pass
    else:
        form = LoginForm()
        context = {"form": form}
        html_template = loader.get_template("accounts/login.html")
        return HttpResponse(html_template.render(context, request))


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password != password_confirmation:
                messages.error(request, "The passwords do not match")
                form = SignupForm()
                context = {"form": form}
                # html_template = loader.get_template('registration/signup.html')
                return render(request, "registration/signup.html", context)
            user = User.objects.create_user(
                username=username, password=password
            )
            login(request, user)
            return redirect(reverse("list_projects"))

        else:
            form = SignupForm()
            context = {"form": form}
            html_template = loader.get_template("registration/signup.html")
            return HttpResponse(html_template.render(context, request))
    else:
        form = SignupForm()
        context = {"form": form}
        html_template = loader.get_template("registration/signup.html")
        return HttpResponse(html_template.render(context, request))
